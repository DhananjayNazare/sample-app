(function () {
  angular.module('sampleApp')
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/home');

      $stateProvider
        .state('home', {
          url: '/home',
          controller: 'homeCtrl',
          template: require('./home.html')
        })
        .state('home.childA', {
          url: '/childA',
          controller: 'childAController',
          template: require('./childA/childA.html')
        })
        .state('home.childB', {
          url: '/childB',
          controller: 'childBController',
          template: require('./childB/childB.html')
        });
    });
})();