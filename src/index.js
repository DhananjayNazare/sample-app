require('sample-lib');
require('angular-ui-router');
require('./styles.less');
require('./app');
require('./home/homeController.js');
require('./home/childA/childAController');
require('./home/childB/childBController');
require('./home/homeRoutingConfig')
angular.bootstrap(document, ['sampleApp']);